// Author : __MinoPB__   
// environment : SCILAB 
// Date of creation: 2019-10-25

function PManalisys()

fid08 = 'PManalysis\data\dane-pomiarowe_2019-10-08.csv'  //ascription a data location to variables
fid09 = 'PManalysis\data\dane-pomiarowe_2019-10-09.csv'
fid10 = 'PManalysis\data\dane-pomiarowe_2019-10-10.csv'
fid11 = 'PManalysis\data\dane-pomiarowe_2019-10-11.csv'
fid12 = 'PManalysis\data\dane-pomiarowe_2019-10-12.csv'
fid13 = 'PManalysis\data\dane-pomiarowe_2019-10-13.csv'
fid14 = 'PManalysis\data\dane-pomiarowe_2019-10-14.csv'


hours = csvRead(fid08, ';', ',', 'string', [], [],[2,1,25,1]) // createing matrix for string type elements of file - houers of                                                                 measurments
mclose('all')


data08 = csvRead(fid08, ';', ',', [], [], [],[2,6,25,6])     // creating matrix of measurments values for each day
mclose('all')

data09 = csvRead(fid09, ';', ',', [], [], [],[2,6,25,6])
mclose('all')

data10 = csvRead(fid10, ';', ',', [], [], [],[2,6,25,6])
mclose('all')

data11 = csvRead(fid11, ';', ',', [], [], [],[2,6,25,6])
mclose('all')

data12 = csvRead(fid12, ';', ',', [], [], [],[2,6,25,6])
mclose('all')

data13 = csvRead(fid13, ';', ',', [], [], [],[2,6,25,6])
mclose('all')

data14 = csvRead(fid14, ';', ',', [], [], [],[2,6,25,6])
mclose('all')

[m08, koor08] = max(data08)                                 // looking for maximu value in each day
[m09, koor09] = max(data09)
[m10, koor10] = max(data10)
[m11, koor11] = max(data11)
[m12, koor12] = max(data12)
[m13, koor13] = max(data13)
[m14, koor14] = max(data14)



max_list = [m08 m09 m10 m11 m12 m13 m14]                        
[MaxT, Day] = max(max_list)                                     //looking for maximum value from whole week
fid_results = 'PManalysis\data\PMresults.dat'                   // lokation of creating a file with results of analysis
[fd, err] = mopen(fid_results, 'a')                             // creating a new file and open to append a new values
info2 = 'o godzinie'
select MaxT                                                     //creating of result file's content
    
    case m08 then
        time = hours(koor08)
        info = 'maksymalne stężenie PM10[Âµg/m3] w badanym tygodniu (08-14.10.2019),odnotowano w dniu 08.10.2019 i wyniosło'
        disp(info)
        disp(MaxT)
        disp(info2)
        disp(time)
        save(fid_results, [ 'info', 'MaxT', 'info2', 'time'])
        disp('wynik zapisano w pliku PMreults.dat')
        mclose('all')

    case m09 then
        time = hours(koor09)
        info = 'maksymalne stężenie PM10[Âµg/m3] w badanym tygodniu(08-14.10.2019), odnotowano w dniu 09.10.2019 i wyniosło'
        disp(info) 
        disp(MaxT)
        disp(info2)
        disp(time)
        save(fid_results, [ 'info', 'MaxT', 'info2', 'time'])
        disp('wynik zapisano w pliku PMreults.dat')
        mclose('all')
        
    case m10 then
        time = hours(koor10)
        inop = 'maksymalne stężenie PM10[Âµg/m3] w badanym tygodniu(08-14.10.2019), odnotowano w dniu 10.10.2019 i wyniosło'
        disp(info) 
        disp(MaxT)
        disp(info2)
        disp(time)
        save(fid_results, info, MaxT, info2, time)
        disp('wynik zapisano w pliku PMreults.dat')
        mclose('all')
             
    case m11 then
        time = hours(koor11)
        info = 'maksymalne stężenie PM10[Âµg/m3] w badanym tygodniu(08-14.10.2019), odnotowano w dniu 11.10.2019 i wyniosło'
        disp(info)
        disp(MaxT)
        disp(info2)
        disp(time)
        save(fid_results, info, MaxT, info2, time)
        disp('wynik zapisano w pliku PMreults.dat')
        mclose('all')
        
    case m12 then
        time = hours(koor12)
        disp('maksymalne stężenie PM10[Âµg/m3] w badanym tygodniu(08-14.10.2019), odnotowano w dniu 12.10.2019 i wyniosło') 
        disp(MaxT)
        disp(info2)
        disp(time)
        save(fid_results, info, MaxT, info2, time)
        disp('wynik zapisano w pliku PMreults.dat')
        mclose('all')
        
    case m13 then
        time = hours(koor13)
        info = 'maksymalne stężenie PM10[Âµg/m3] w badanym tygodniu(08-14.10.2019), odnotowano w dniu 13.10.2019 i wyniosło'
        disp(info)
        disp(MaxT)
        disp(info2)
        disp(time)
        save(fid_results, info, MaxT, info2, time)
        disp('wynik zapisano w pliku PMreults.dat')
        mclose('all')
        
    case m14 then
        time = hours(koor14)
        info = 'maksymalne stężenie PM10[Âµg/m3] w badanym tygodniu(08-14.10.2019), odnotowano w dniu 14.10.2019 i wyniosło'
        disp(info)
        disp(MaxT)
        disp(info2)
        disp(time)
        save(fid_results, info, MaxT, info2, time)
        disp('wynik zapisano w pliku PMreults.dat')
        mclose('all')
        
end

medium = zeros(7,2)    
for j=1:24                                 //calculation of medium values of PM10 concentration ...
    q = j>=7
    e = j<= 18
    if  q+e == 2                             //... for day                
    medium(1,1) = medium(1,1) + data08(j)/11 
    medium(2,1) = medium(2,1) + data09(j)/11
    medium(3,1) = medium(3,1) + data10(j)/11
    medium(4,1) = medium(4,1) + data11(j)/11
    medium(5,1) = medium(5,1) + data12(j)/11
    medium(6,1) = medium(6,1) + data13(j)/11
    medium(7,1) = medium(7,1) + data14(j)/11
    else                                       // and for night
    medium(1,2) = medium(1,2) + data08(j)/13
    medium(2,2) = medium(2,2) + data09(j)/13
    medium(3,2) = medium(3,2) + data10(j)/13
    medium(4,2) = medium(4,2) + data11(j)/13
    medium(5,2) = medium(5,2) + data12(j)/13
    medium(6,2) = medium(6,2) + data13(j)/13
    medium(7,2) = medium(7,2) + data14(j)/13 
    end
end

Med = sum(medium, 'r') / 7                      // calucation of medimum concentration's value in whole week...
devD = medium(1:7,1)                            // ..for days
devN = medium(1:7,2)                            //.. for nights

nominator = abs(Med(1)- Med(2))                         
denominator = sqrt(stdev(devD)^2/7 + stdev(devN)^2/7)
LRes = nominator/denominator                            // carry out an statistick test for diference's relevance

if LRes<2 then                                          //creating result's file content
    com = 'ponieważ wynik testu statystycznego jest mniejszy niż 2, można przyjąć, że nie występuje istotna statystycznie różnic    a pomiędzy wynikami z dnia i nocy'
    disp(com)
    [fd, err] = mopen(fid_results, 'w')
    save(fid_results, [ 'LRes','com'])
    mclose('all')
else
    com = 'ponieważ wynik testu statystycznego jest większy 2, można przyjąć, że występuje istotna statystycznie różnica pomiędz    y wynikami z dnia i nocy'
    disp(com)
    [fd, err] = mopen(fid_results, 'a')
    save(fid_results, [ 'LRes','com'])
    mclose('all')
end

endfunction
